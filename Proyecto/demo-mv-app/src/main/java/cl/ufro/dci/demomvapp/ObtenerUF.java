package cl.ufro.dci.demomvapp;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ObtenerUF {
    public static final String URL = "https://si3.bcentral.cl/indicadoressiete/secure/Serie.aspx?gcode=UF&param=RABmAFYAWQB3AGYAaQBuAEkALQAzADUAbgBNAGgAaAAkADUAVwBQAC4AbQBYADAARwBOAGUAYwBjACMAQQBaAHAARgBhAGcAUABTAGUAYwBsAEMAMQA0AE0AawBLAF8AdQBDACQASABzAG0AXwA2AHQAawBvAFcAZwBKAEwAegBzAF8AbgBMAHIAYgBDAC4ARQA3AFUAVwB4AFIAWQBhAEEAOABkAHkAZwAxAEEARAA=";

    public List<UF> ObtenerUF() {
            List<UF> ufs = new ArrayList<>();
            Document documento;
            try{
                documento = Jsoup.connect(URL).get();
            }catch (IOException e){
                throw new RuntimeException(e);
            }
            Elements table = documento.select("tr").not(".GridHeader");
            for (Element row : table.subList(1, table.size())){
                String day = row.select("td").first().text();
                int mes = 0;
                Elements precios = row.getElementsByClass("obs");
                for (Element precio :precios){
                    UF uf = new UF();
                    mes++;
                    if (!precio.text().isEmpty()){
                        String precioString = precio.text();
                        String newPrecio = precioString.replace( ".","");
                        String newPrecio1 = newPrecio.replace( ",",".");
                        System.out.println(newPrecio1);
                        uf.setPrecio(Double.parseDouble(newPrecio1));
                        uf.setFecha(day + "/" + mes + "/2021");
                        ufs.add(uf);
                    }
                }
            }
            return ufs;
        }
        public void mostrarUf(List<UF> ufs){
            for (int i = 0; i < ufs.size(); i++) {
                System.out.println("Fecha : " + ufs.get(i).getFecha() + ";" + "Precio : " + ufs.get(i).getPrecio());
            }
        }
    }
